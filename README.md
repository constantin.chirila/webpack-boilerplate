# Webpack Frontend Boilerplate

Basic starter-kit for a front-end project. 


### Installation

```
npm install
```

### Start Developer Server

```
npm start
```

### Build Produdction Version

```
npm run build
```

### Features:

* Supports ES6 with [babel-loader](https://github.com/babel/babel-loader)
* Supports SASS with [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting provided with [eslint-loader](https://github.com/MoOx/eslint-loader)

On `npm run build` it uses the [extract-text-webpack-plugin](https://github.com/webpack/extract-text-webpack-plugin) to move the css to a separate file and include it in the head of the `index.html`, so that the styles are applied before any javascript is loaded. This function is disabled in the dev version, because the loader doesn't support hot module replacement.

### Support

For support or questions: <hello@constantinchirila.com>