'use strict';

const Path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const fs = require('fs')

const dest = Path.join(__dirname, '../dist');

function generateHtmlPlugins (templateDir) {
  const templateFiles = fs.readdirSync(Path.resolve(__dirname, templateDir))
  let objects =  templateFiles.map(item => {
    const parts = item.split('.')
    const name = parts[0]
    if(parts[1]) {
      const extension = parts[1]
      return new HtmlWebpackPlugin({
        filename: `${name}.html`,
        template: Path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
      })
    }
  })
  return objects = objects.filter(function(n){ return n != undefined }); 
}

module.exports = {
  entry: [
    Path.resolve(__dirname, '../src/scripts/index')
  ],
  output: {
    path: dest,
    filename: 'bundle.[hash].js'
  },
  plugins: [
    new CleanWebpackPlugin([dest]),
    new CopyWebpackPlugin([
      { from: Path.resolve(__dirname, '../public'), to: 'public' }
    ]),
    // new HtmlWebpackPlugin({
    //   template: Path.resolve(__dirname, '../src/index.html')
    // }),  
  ].concat(generateHtmlPlugins('../src/')),
  resolve: {
    alias: {
      '~': Path.resolve(__dirname, '../src')
    }
  },
  module: {
    rules: [
      {
        test: /\.(ico|jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }
      }
    ]
  }
};
